rootProject.name = "magic"


include(":api")
include(":infra")
include(":app")
include(":web")
include(":service:files")
include(":service:platform")
