package com.github.blanexie.magic.service.platform.service

import com.github.blanexie.magic.platform.entity.Result


/**
 *
 * @author xiezc
 * @date 2024/8/28 14:43
 */
interface ResultService {

    fun save(result: Result)

}